import React, {Component} from 'react';
import {BrowserRouter as Router,Switch, Route,Redirect} from 'react-router-dom';
import HomeContainer from '../containers/home/HomeContainer'
import LoginContainer from '../containers/registration/loginContainer'
import SignupContainer from '../containers/registration/signupContainer'
import ConfirmSignupContainer from '../containers/registration/confirmSignupContainer'
import ForgotPasswordContainer from '../containers/registration/forgotPasswordContainer'

class AppRoute extends Component {
    render() {
        
        return (
            <Router>
            <Switch>
              <Route exact path="/" component={HomeContainer}/>
              <Route path="/login" component={LoginContainer} />
              <Route path='/signup' component={SignupContainer}/>
              <Route path='/confirm-signup/:id/:token' component={ConfirmSignupContainer}/>
              <Route path='/accounts/password-reset/' component={ForgotPasswordContainer}/>
            </Switch>
          </Router>
        );
    }
}
export default AppRoute;