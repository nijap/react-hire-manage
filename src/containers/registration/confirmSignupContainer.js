import React, { Component } from 'react';
import ConfirmSignup from '../../components/registration/confirmSignup';
import emailVerification from '../../redux/actions/emailVerificationAction';
import registrationComplete from '../../redux/actions/registrationCompletionAction';
import { connect } from "react-redux";


const mapStateToProps = state=>{
    return{
        isEmailVerified:state.emailVerificationData,
    }
}

const mapDispatchToProps = dispatch => ({

    emailVerification: (verificationData) => dispatch(emailVerification(verificationData)),
    registrationComplete: (registrationData) => dispatch(registrationComplete(registrationData))
})

export default connect(mapStateToProps,mapDispatchToProps)(ConfirmSignup)