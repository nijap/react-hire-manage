import React ,{ Component } from 'react';
import { connect } from "react-redux";
import Signup from '../../components/registration/Signup'
import userSignUp from '../../redux/actions/signupAction';


const mapStateToProps = state=>{
    return{
        userSignupDetails:state.userSignup,
    }
}

const mapDispatchToProps = dispatch => ({

    userSignUp: (signUpData) => dispatch(userSignUp(signUpData)),
    // resetSignup: () => dispatch(reset('signupform'))
})

export default connect(mapStateToProps,mapDispatchToProps)(Signup)