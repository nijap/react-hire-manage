import React, { Component } from 'react';
import Header from '../../components/header/Header';
import Homebody from '../../components/home/HomePageBody';
import Footer from '../../components/footer/Footer';

export default class HomeContainer extends Component{
    render(){
        return(
            <div>
                <Header login={true} showsubheader={true}/>
                <Homebody/>
                <Footer/>
            </div>
        )
    }
}