import React ,{ Component } from 'react';


export default class Homebody extends Component{
    render(){
        return(
        <section class="slider-wrapper fullwidth pull-left position-relative d-table">
            <div class="overlay"></div>
            
            <div class="slider-content d-table-cell align-middle position-relative text-white">
                <div class="container">
                    <h1>Lorem Ipsum</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,<br /> sed do eiusmod tempor incididunt  </p>
                    <button type="button" class="bg text-white fullwidth pt-2 pb-2">Get Started</button>
                </div>
            </div>
        </section>
        )
    }
}