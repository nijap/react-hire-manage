import React ,{ Component, Fragment } from 'react';
import LoaderImage from '../../assets/images/loader.gif';


export default class Loader extends Component{
    render(){
        return(
            <Fragment>
            <div className="loader-img"> <img src={LoaderImage} className="mx-auto d-block center" style={{ width: "20%" }} alt="Leap" /></div>
            </Fragment>
        )
    }
}