import React ,{ Component } from 'react';
import { Link } from 'react-router-dom';


export default class Header extends Component{
    

    render(){
        return(
        <div className="pull-left fullwidth">
            <div className="header-wrapper fullwidth pull-left pt-4 pb-4">
                <div className="container">
                    <div className="logo pull-left">
                        <Link to= '/'>
                        <img className="img-fluid" src={require("../../assets/images/logo.png") }/>
                        </Link>
                    </div>
                    
                    { this.props.login ? 
                    <div className="pull-right hdr-rght">
                        <Link to='/login'
                         href="" className="text-uppercase mar-right color">log in
                         </Link>                        
                        <Link to= "/signup" 
                        className="text-uppercase color">sign up
                        </Link>
                    </div> 
                    : null
                    }
                
                    

                </div>
            </div>
            {
                this.props.showsubheader ? 
                <div className="menu-wrapper fullwidth pull-left">
                    <div className="container">
                        <ul className="fullwidth text-center m-0">
                            <li className="d-inlin li-list"><a href="">Home</a></li>
                            <li className="d-inlin li-list"><a href="">About Us</a></li>
                            <li className="d-inlin li-list"><a href="">Services</a></li>
                            <li className="d-inlin li-list"><a href="">Contact Us</a></li>
                            <li className="li-list dis-none"><a href="">LOG IN</a></li>
                            <li className="li-list dis-none"><a href="">SIGN UP</a></li>
                        </ul>
                    </div>
                </div>
                : null
            }
            

        </div>
        )
    }
}


