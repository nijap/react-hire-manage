import React ,{ Component } from 'react';
import { Link } from 'react-router-dom';


export default class Footer extends Component{
    render(){
        return(
            <footer class="pull-left fullwidth">
            <div class="footer-wrapper fullwidth pull-left">
                <div class="container">
                    <div class="un-line fullwidth pull-left pt-3 pb-3">
                        <ul class="pull-left text-white">
                            <li class="text-uppercase ftr-blk"><a class="ft-link" href="">about us</a></li>
                            <Link class="text-uppercase ftr-blk ft-link" to="/login">log in</Link>
                            <Link class="text-uppercase ftr-blk ft-link" to="/signup">sign up</Link>
                            <li class="text-uppercase ftr-blk"><a class="ft-link" href="">contact us</a></li>
                        </ul>
                        <ul class="pull-right m-0">
                            <div class="soc-mid">
                                <li class="d-inline soc-list mr-3 pull-left"><a href="" class="pull-left"><i class="fa fa-facebook-f"></i></a></li>
                                <li class="d-inline soc-list mr-3 pull-left"><a href="" class="pull-left"><i class="fa fa-linkedin"></i></a></li>
                                <li class="d-inline soc-list pull-left"><a href="" class="pull-left"><i class="fa fa-twitter"></i></a></li>
                            </div>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="copyright fullwidth text-center pt-3 pb-3 pull-left text-white"> &copy;2019 HIRE & MANAGE.</div>
        </footer>
        )
    }
}