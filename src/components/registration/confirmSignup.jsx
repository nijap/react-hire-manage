import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import { Field, reduxForm } from 'redux-form'
import Loader from '../general/Loader'
import Header from '../../components/header/Header';
import Footer from '../../components/footer/Footer';


const passwordsMustMatch = (value, allValues) => {
  return(value !== allValues.password1 ? 
    ' do not match' :
     undefined)
}

const radioCheckbox = ({ input,options,type, meta: { touched, error, warning } }) => (
    <Fragment>
        <div class="sgn-md-btn fullwidth pull-left">
            {options.map((plan, index) => (
                // <label key={index} className="radio-button radio-button-css">
                //     <input type="radio" {...input} key={index} value={plan.value} /> 
                //     <span class="label-visible subscribe_radio">
                //         <span class="fake-radiobutton"></span>
                //         <strong>
                //             <b>{plan.text}</b>
                //             <small>${plan.value}/year</small>
                //         </strong>
                //     </span>
                // </label>
                <div className="col-md-6 pull-left">
                    <div className="row">
                        <input type="radio" {...input} key={index} name={plan.name} value={plan.value} id={plan.id}  />
                        {/* <Field name="registration_type" component="input" type="radio" value={plan.value} id="id_registration_type_0"/> */}
                        <label for={plan.id} class="fullwidth pull-left">{plan.text}</label>
                    </div>
                </div>
                ))}
        </div>
        <label>{touched &&
            ((error && <span style={{ color: "red", fontSize: "14px" }}>{error}</span>) ||
                (warning && <span>{warning}</span>))}</label>
    </Fragment>
)

const Checkbox = ({ input,className, meta: { touched, error, warning } }) => (
    <Fragment>
        <div>
            <input type="checkbox" className={className} {...input} />Yes, I understand and agree to the <a href="" className="clr fnt-600">Terms of Service</a>, including the <a href="" className="clr fnt-600">User Agreement</a> and <a href="" className="clr fnt-600">Privacy Policy</a>.
        </div>
        {touched &&
            ((error && <span style={{ color: "red", fontSize: "14px" }}>{error}</span>) ||
                (warning && <span>{warning}</span>))}
    </Fragment>
)

const required =  value => {
    return(value || typeof value === 'number' ? undefined : ' is Required')
}

const requiredRadio =  value => {
    return(value || typeof value === 'number' ? undefined : ' Select a  Service')
}

const requiredCheck =  value => {
    return(value || typeof value === 'number' ? undefined : ' Please read the terms and tick')
}


const minLength = min => value =>
    value && value.length < min ? ` Must be ${min} characters or more` : undefined
const minLength2 = minLength(8)

const renderField = ({
    input,
    label,
    type,
    existed,
    meta: { touched, error, warning,  }
  }) => {
      
    return(
    

      <Fragment>
        <input {...input} placeholder={label} type={type} className={(touched && error)||(existed&&existed.existedMail) ? 'fullwidth border border-danger': 'fullwidth'}/>
        {touched &&
          ((error && <small className="text-danger">{label}{error}</small>) ||
            (warning && <span>{warning}</span>))}
        {((existed)&&(existed.existedMail)) && <small class="text-danger">
            {existed.error.data.user.email[0]}
        </small>}
        {/* {((existed)&&(existed.error.status == 500)) && <small class="text-danger">
            {existed.errors.error}
        </small>} */}
        
      </Fragment>
    
  )}





class ConfirmSignup extends Component {

    componentDidMount() {
        if (this.props.match.params) {
            this.props.emailVerification(this.props.match.params);
        }
    }

    submit = (values) => {
        values.token = this.props.isEmailVerified.message.token
        values.profile_slug = this.props.isEmailVerified.message.profile_slug

        // this.props.resetForm()
        this.props.registrationComplete(values)
    }
    handleInputChange = (e) => {
        const name = e.target.name;
        const value = e.target.value;

        // this.setState({
        //     formControls: {
        //         ...this.state.formControls,
        //         [name]: {
        //             ...this.state.formControls[name],
        //             value
        //         }
        //     }
        // });
    }

    render() {
        const { handleSubmit, invalid,pristine, reset, submitting } = this.props
        const serviceType = [
            { text: 'Employer', value: 'employer',name:"registration_type" , id:"id_registration_type_0"},
            { text: 'Service Company', value: 'service_company',name:"registration_type" , id:"id_registration_type_1" }
        ]
        return (
            <div>
            {(this.props.isEmailVerified) ?
                <Fragment>
                    <Header/>
                <section class="login-wrapper signup-wrapper fullwidth pull-left position-relative">
                    <div class="overlay2"></div>
                    <div class="login m-auto fullwidth bg-white d-table position-relative">
                        <div class="pad-48">
                            <h5 class="text-uppercase text-center mb-2 font-18">Complete your free account setup</h5>
                            <p class="fullwidth text-center fnt-14">{(this.props.isEmailVerified.message)&&this.props.isEmailVerified.message.email}<i class="icon-checked b-6 text-success"></i></p>
                            <div class="alert fade show alert-warning-green mt-3">
                                <i class="icon-checked b-6 alert-icon mr-2"></i> Your registered email address is verified successfully.
                            </div>
                            <form onSubmit={handleSubmit(this.submit)} noValidate>
                                <div class="col-md-12 pull-left log-icon signup">
                                    {/* <select class="fullwidth">
                                        <option value="Select your country">Select your country</option>
                                        <option value="India">India</option>
                                        <option value="Australia">Australia</option>
                                        <option value="USA">USA</option>
                                    </select> */}
                                    <Field name="country" component="select" className="fullwidth">
                                        <option value="Select your country">Select your country</option>
                                        <option value="India">India</option>
                                        <option value="Australia">Australia</option>
                                        <option value="USA">USA</option>
                                    </Field>
                                </div>

                                <div class="col-md-12 pull-left log-icon signup">
                                    {/* <input type="password" name="password1" placeholder="Password" class="input-control fullwidth" required="" id="id_password1" /> */}
                                    <Field
                                        name="password1"
                                        type="password"
                                        label="Password"
                                        placeholder="Password"
                                        onChange={this.handleInputChange}
                                        className="input-control fullwidth"
                                        component={renderField}
                                        validate={[required,minLength2]}
                                    />
                                </div>

                                <div class="col-md-12 pull-left log-icon signup">
                                    {/* <input type="password" name="password2" placeholder="Confirm Password" class="input-control fullwidth" required="" id="id_password2" /> */}
                                    <Field
                                        name="password2"
                                        type="password"
                                        label="Confirm Password"
                                        placeholder="Confirm Password"
                                        onChange={this.handleInputChange}
                                        className="input-control fullwidth"
                                        component={renderField}
                                        validate={[required,passwordsMustMatch]}
                                    />
                                </div>


                                <div class="sign-mid fullwidth text-center">
                                    <h6 class="fnt-600 mb-3">I want to register as:</h6>
                                    {/* <div class="sgn-md-btn fullwidth pull-left"> */}
                                        {/* <div class="col-md-6 pull-left">
                                            <div class="row">
                                                <input type="radio" name="registration_type" value="employer" id="id_registration_type_0" required />
                                                <Field name="registration_type" component="input" type="radio" value="employer" id="id_registration_type_0"/>
                                                <label for="id_registration_type_0" class="fullwidth pull-left">Employer</label>
                                            </div>
                                        </div>
                        
                                        <div class="col-md-6 pull-left">
                                            <div class="row">
                                                <input type="radio" name="registration_type" value="service_company" id="id_registration_type_1" required checked />
                                                <Field name="registration_type" component="input" type="radio" value="service_company" id="id_registration_type_1"/>
                                                <label for="id_registration_type_1" class="fullwidth pull-left">Service Company</label>
                                            </div>
                                        </div> */}
                                        <Field
                                        name="registration_type"
                                        component={radioCheckbox}
                                        options = {serviceType}
                                        validate={requiredRadio}
                                        onChange={this.handleRadioButtonChange}/>
                        
                                    {/* </div> */}
                                </div>

                                <div class="sgn-chk-bx fullwidth pull-left fnt-12 mb-4 mt-4">
                                    <Field name="policyagree" id="employed" component={Checkbox} validate={requiredCheck} type="checkbox" className="mr-2" />
                                    {/* <input type="checkbox" class="mr-2" />Yes, I understand and agree to the <a href="" class="clr fnt-600">Terms of Service</a>, including the <a href="" class="clr fnt-600">User Agreement</a> and <a href="" class="clr fnt-600">Privacy Policy</a>. */}
                                </div>
                                <button class="bg fullwidth text-center text-capitalize lg-btn sgn-btn d-table m-auto text-white pt-1 pb-1">Create my Account</button>
                                <p class="fullwidth text-center signup-p">Already have an account? <a href="" class="clr fnt-600"><Link to='/login'>Log In</Link></a></p>
                            </form>
                        </div>
                    </div>
                </section>
                <Footer/>
                </Fragment>
                :<Loader/>
            }
            </div>
        )}
}

ConfirmSignup = reduxForm({
    form:'confirmSignup'
})(ConfirmSignup)
export default ConfirmSignup