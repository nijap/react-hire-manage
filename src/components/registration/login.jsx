import React ,{ Component ,Fragment} from 'react';
import { Link } from 'react-router-dom';
import { Field, reduxForm } from 'redux-form'
import Footer from '../footer/Footer';
import Header from '../header/Header';



const required =  value => {
    return(value || typeof value === 'number' ? undefined : ' is Required')
}

const minLength = min => value =>
    value && value.length < min ? ` Must be ${min} characters or more` : undefined
const minLength2 = minLength(8)

const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? ' is Invalid'
    : undefined




const renderField = ({
    input,
    label,
    type,
    existed,
    meta: { touched, error, warning,  }
  }) => {
      
    return(
    

      <Fragment>
        <input {...input} placeholder={label} type={type} className={(touched && error)||(existed&&existed.existedMail) ? 'fullwidth border border-danger': 'fullwidth'}/>
        {touched &&
          ((error && <small className="text-danger">{label}{error}</small>) ||
            (warning && <span>{warning}</span>))}
        {((existed)&&(existed.existedMail)) && <small class="text-danger">
            {existed.error.data.user.email[0]}
        </small>}
        {/* {((existed)&&(existed.error.status == 500)) && <small class="text-danger">
            {existed.errors.error}
        </small>} */}
        
      </Fragment>
    
  )}

class Login extends Component{
    submit = (values) => {
        console.log("login values: ",values)
        // this.props.history.push('/signup')
    }

    render(){
        const { handleSubmit, pristine, reset, submitting } = this.props
        return(
            <Fragment>
            <Header />
            <div className="login-wrapper fullwidth pull-left">
                <div className="login m-auto fullwidth bg-white d-table">
                    <div className="pad-48">
                        <h5 className="text-uppercase text-center mb-5">Log in and get to work</h5>
                        <form onSubmit={handleSubmit(this.submit)} noValidate>
                            <div className="position-relative log-icon mb-4">
                                <span className="position-absolute clr"><i className="icon-avatar"></i></span>
                                {/* <input type="email" placeholder="User Name or Email" className="fullwidth" /> */}
                                <Field type="email" name="email" component={renderField} 
                                label="Email Address" existed ={this.props.userSignupDetails} validate={[required,email]} />
                            </div>
                            <div className="position-relative log-icon mb-2">
                                <span className="position-absolute clr"><i className="fa fa-unlock-alt"></i></span>
                                {/* <input type="password" placeholder="Password"  className="fullwidth" /> */}
                                <Field
                                    name="password1"
                                    type="password"
                                    label="Password"
                                    placeholder="Password"
                                    onChange={this.handleInputChange}
                                    className="input-control fullwidth"
                                    component={renderField}
                                    validate={[required,minLength2]}
                                />
                            </div>
                            <div className="log-lnk fullwidth pull-left mb-4">
                                <div className="pull-left"><input className="mr-2" type="checkbox" />Keep me logged in</div>
                                <a href="" className="pull-right"><Link to="/accounts/password-reset/">Forgot Password</Link></a>
                            </div>
                            <button className="bg fullwidth text-center text-capitalize lg-btn d-table m-auto text-white pt-1 pb-1">log in</button>
                        </form>
                    </div>
                    <div className="log-sign-up fullwidth pull-left position-relative">
                        <div className="overlay2"></div>
                        <div className="position-relative">
                            <h6 className="text-white text-center mb-3">New to Hire & Manage</h6>
                            <Link to='signup/'>
                            <button className="bg-white fullwidth text-center text-capitalize lg-btn d-table m-auto pt-1 pb-1 clr border-white"><a href="">sign up</a></button>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
            <Footer/>
            </Fragment>
        )
    }
}

Login = reduxForm({
    form:'loginform'
})(Login)
export default Login