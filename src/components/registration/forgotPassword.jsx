import React ,{ Component ,Fragment} from 'react';
import { Link } from 'react-router-dom';
import { Field, reduxForm } from 'redux-form'
import Footer from '../footer/Footer';
import Header from '../header/Header';



class ForgotPassword extends Component{
    render(){
        return(
            <Fragment>
                <div className="container-fluid">
                    <div className="row full-height justify-content-center">
                        <div className="col-sm-6 col-md-3 align-self-center">
                        <img src="/static/assets/image/logo.png" className="brand-img"/>

                            

                            

                        <div className="bx-content shadow mt-3 pt-5 pb-5">
                            <div className="col-sm-12 pl-4 pr-4 pt-4 pb-2 text-center"><h5>FORGOT PASSWORD</h5></div>
                            <div className="col-sm-12 pl-4 pr-4">

                            <form method="POST" action="." novalidate="">
                                    <input type="hidden" name="csrfmiddlewaretoken" value="2mIw8sWK2jUMXociRGgomtSq1Cx59IhoFB3tZUEowztKtJo1hLReQd6R2cvwSVco"/>
                                <div className="row">

                                    <div className="col-sm-12 mb-2">
                                        <label className="text-light-grey">Email Address</label>

                                        <input type="email" name="email" maxlength="254" id="id_email" className="input-control
                                        " placeholder="Enter Email Address"/>

                                        

                                    </div>
                                    
                                    <div className="col-sm-12 text-right">
                                        <button type="submit" className="btn btn-primary">Submit</button>
                                    </div>

                                </div>
                            </form>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                                
            </Fragment>
        )
    }
}

// Login = reduxForm({
//     form:'loginform'
// })(Login)
export default ForgotPassword