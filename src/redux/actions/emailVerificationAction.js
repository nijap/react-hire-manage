import * as emailType from '../../constants/actionConstants';
// import { performRequest } from '../../config/index';
import axios from 'axios'

export default function emailVerification(urlData){

    return function (dispatch) {
        //return  axios.post("http://8c5389eb.ngrok.io/users",postReqObj)

        // return performRequest('get', 'https://hm-rest.herokuapp.com/accounts/email-confirmation/'+urlData.id+'/'+urlData.token, postReqObj)
        return axios.get('https://hm-rest.herokuapp.com/accounts/email-confirmation/'+urlData.id+'/'+urlData.token+'/')    
            .then((response) => {
                // if (response.data.message == "success") {
                //     dispatch(getAccessToken(postReqObj, response.data.id));
                // }
                dispatch({
                    type: emailType.EMAIL_VERIFY_SUCCESS,
                    payload: response
                })
                localStorage.setItem("token", response.data.token)
                localStorage.setItem("profile_slug", response.data.profile_slug)
            })
            .catch((error) => {
                dispatch({
                    type: emailType.EMAIL_VERIFY_FAILURE,
                    payload: error
                })
            })
    }
}