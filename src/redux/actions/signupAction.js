import * as signupType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import {reset} from 'redux-form';

export default function userSignUp(signUpData){

    let postReqObj = {};
    postReqObj.user = {};
    postReqObj.user.email=signUpData.email;
    postReqObj.first_name=signUpData.firstname;
    postReqObj.last_name=signUpData.lastname;
    postReqObj.activation_url = "http://localhost:3000/confirm-signup/"
    const headers ={"Content-Type": "application/json"}

    return function (dispatch) {
        //return  axios.post("http://8c5389eb.ngrok.io/users",postReqObj)

        return performRequest('post', 'https://hm-rest.herokuapp.com/accounts/signup/',headers, postReqObj)
            .then((response) => {
                // if (response.data.message == "success") {
                //     dispatch(getAccessToken(postReqObj, response.data.id));
                // }
                dispatch({
                    type: signupType.ADD_USER_SUCCESS,
                    payload: response
                })
                dispatch(reset('signupform'))
                // localStorage.setItem("userId", response.data.data[0].userId);
            })
            .catch((error) => {
                dispatch({
                    type: signupType.ADD_USER_FAILURE,
                    payload: error
                })
            })
    }
}