import * as regType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
// import axios from 'axios'

export default function registrationComplete(completionData){

    let postReqObj = {};
    postReqObj.profile_data = {};
    postReqObj.password_data = {};

    postReqObj.profile_slug=completionData.profile_slug;

    postReqObj.profile_data.country="105";
    postReqObj.profile_data.registration_type=completionData.registration_type;
    postReqObj.profile_data.accepted=completionData.policyagree;

    postReqObj.password_data.password=completionData.password1;
    postReqObj.password_data.confirm_password=completionData.password2;

    const token = completionData.token
    const headers ={"Content-Type": "application/json","Authorization":"Token "+token}

    return function (dispatch) {
        //return  axios.post("http://8c5389eb.ngrok.io/users",postReqObj)

        return performRequest('post', 'https://hm-rest.herokuapp.com/accounts/complete-registration/',headers, postReqObj)
        // return axios.get('https://hm-rest.herokuapp.com/accounts/email-confirmation/'+urlData.id+'/'+urlData.token+'/')    
            .then((response) => {
                dispatch({
                    type: regType.REGISTRATION_COMPLETION_SUCCESS,
                    payload: response
                })
            })
            .catch((error) => {
                dispatch({
                    type: regType.REGISTRATION_COMPLETION_FAILURE,
                    payload: error
                })
            })
    }
}