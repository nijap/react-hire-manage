import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form'
import {emailVerificationReducer} from './emailVerificationReducer'
import {signupReducer} from './signupreducer'

const rootReducer = combineReducers({
    form: formReducer,
    userSignup:signupReducer,
    emailVerificationData:emailVerificationReducer
})
export default rootReducer