import * as emailType from '../../constants/actionConstants';

export function emailVerificationReducer(state = null, action) {
    switch (action.type) {
        case emailType.EMAIL_VERIFY_REQUEST:
            return { ...state };

        case emailType.EMAIL_VERIFY_SUCCESS:
                // localStorage.setItem("token", action.payload.data.token);

            return {
                ...state, message: action.payload.data
            }

        case emailType.EMAIL_VERIFY_FAILURE:
            return {
                ...state,
                error: action.payload
            };
        default:
            return state;
    }
}