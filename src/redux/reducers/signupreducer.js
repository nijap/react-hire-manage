import * as signupType from '../../constants/actionConstants';

export function signupReducer(state = null, action) {
    switch (action.type) {
        case signupType.ADD_USER_REQUEST:
            return { ...state };

        case signupType.ADD_USER_SUCCESS:
                // localStorage.setItem("token", action.payload.data.token);

            return {
                ...state, message: action.payload.data
            }

        case signupType.ADD_USER_FAILURE:
            return {
                ...state,
                error: action.payload.response,
                existedMail : "isTrue"
            };
        default:
            return state;
    }
}