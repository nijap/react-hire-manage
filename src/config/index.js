import axios from 'axios'

const API_URL = '';


export const performRequest = (method, url,headers, params, auth) => {
 const body = method === 'get' ? 'params' : 'data'

 const config = {
   method,
   url,
   headers:headers,
   baseURL: API_URL,
   [body]: params || {}
 }

//  if (auth) {
//    config.headers = {
//     //  Authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ZGI2OGY2ODc1ZWE2MzAwMTIwY2RlMmEiLCJlbWFpbCI6ImFydW53aWxzb25AbWFpbGluYXRvci5jb20iLCJwZXJtaXNzaW9uTGV2ZWwiOjIwNDksInByb3ZpZGVyIjoiZW1haWwiLCJuYW1lIjoiQXJ1biBXaWxzb24iLCJyZWZyZXNoS2V5IjoicXVJMTREbDdGb2FuSU9BVXVlbXRjZz09IiwiaWF0IjoxNTcyODUzNjI3fQ.MPQ1ronHtgWzjAy7_wXU7q9ncgBNA6QDsrjLDjJFW-Y'
//    }
//  }else{
//    config.headers ={
//       "Content-Type": "application/json"
//    }
//  }


 return  axios.request(config);
}