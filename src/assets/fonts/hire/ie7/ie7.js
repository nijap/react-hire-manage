/* To avoid CSS expressions while still supporting IE 7 and IE 6, use this script */
/* The script tag referencing this file must be placed before the ending body tag. */

/* Use conditional comments in order to target IE 7 and older:
	<!--[if lt IE 8]><!-->
	<script src="ie7/ie7.js"></script>
	<!--<![endif]-->
*/

(function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'hire\'">' + entity + '</span>' + html;
	}
	var icons = {
		'icon-checked-circle': '&#xe93a;',
		'icon-placeholder': '&#xe918;',
		'icon-printer': '&#xe938;',
		'icon-download': '&#xe939;',
		'icon-add-folder': '&#xe936;',
		'icon-delete': '&#xe937;',
		'icon-worker': '&#xe934;',
		'icon-strategy': '&#xe935;',
		'icon-add-users': '&#xe932;',
		'icon-add-user': '&#xe933;',
		'icon-skype-o': '&#xe900;',
		'icon-skype-1': '&#xe901;',
		'icon-search': '&#xe902;',
		'icon-phone-ringing': '&#xe903;',
		'icon-phone-ringing-o': '&#xe904;',
		'icon-trash': '&#xe905;',
		'icon-pencil': '&#xe906;',
		'icon-envelope': '&#xe907;',
		'icon-paper-clip': '&#xe908;',
		'icon-edit': '&#xe909;',
		'icon-edit-bold': '&#xe90a;',
		'icon-facebook-o': '&#xe90b;',
		'icon-facebook': '&#xe90c;',
		'icon-plus': '&#xe90d;',
		'icon-plus-circle-o': '&#xe90e;',
		'icon-clock-o': '&#xe90f;',
		'icon-calendar': '&#xe910;',
		'icon-avatar': '&#xe911;',
		'icon-menu': '&#xe912;',
		'icon-grid': '&#xe913;',
		'icon-chevron-arrow-left': '&#xe914;',
		'icon-chevron-arrow-right': '&#xe915;',
		'icon-chevron-arrow-down': '&#xe916;',
		'icon-chevron-arrow-up': '&#xe917;',
		'icon-checked-circle-o': '&#xe919;',
		'icon-checked': '&#xe91a;',
		'icon-settings': '&#xe91b;',
		'icon-gear-o': '&#xe91c;',
		'icon-gear': '&#xe91d;',
		'icon-warning-circle': '&#xe91e;',
		'icon-warning-triangle': '&#xe91f;',
		'icon-warning-circle-o': '&#xe920;',
		'icon-warning-triangel-o': '&#xe921;',
		'icon-twitter-o': '&#xe922;',
		'icon-twitter': '&#xe923;',
		'icon-bookmark-o': '&#xe924;',
		'icon-bookmark': '&#xe925;',
		'icon-save': '&#xe926;',
		'icon-cross-circle': '&#xe927;',
		'icon-cross': '&#xe928;',
		'icon-cross-circle-o': '&#xe929;',
		'icon-graph-level': '&#xe92a;',
		'icon-puzzle': '&#xe92b;',
		'icon-group': '&#xe92c;',
		'icon-user-level': '&#xe92d;',
		'icon-bell': '&#xe92e;',
		'icon-bell-o': '&#xe92f;',
		'icon-phone-o': '&#xe930;',
		'icon-phone': '&#xe931;',
		'0': 0
		},
		els = document.getElementsByTagName('*'),
		i, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		c = el.className;
		c = c.match(/icon-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
}());
